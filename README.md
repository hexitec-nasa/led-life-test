README
------
Code to support the LED life test.

INSTALL
-------
To install a developer edition clone this repository and run:

pip install -e .

inside the package directory.

DOCUMENTATION
-------------
Main Files:

__init__.py stores variables and functions

led.py has the LedImage() class that processes the image and finds the LEDs

snap.py takes a batch of photos every day and stores them in a monthly folder

find_regions.py finds the location of each LED in the 10,000 microsecond every month, creates a bounding box around each LED and stores bounding boxes to LEDboxes.txt. Also checks if LED location has changed

image_check_loop.py uses bounding boxes from LEDboxes.txt as LED region in LedImage(), finds the brightness of each LED within those boudning boxes and writes to csv file. Also checks if LED brightness has changed. Runs every day

PlotLeds.py Plots LED flux against image creation date every day for the last month's worth of data

Startup
=======

Temperatures
------------
The temperatures are read out by an arduino which is connected directly
to the computer. The arduino is running the code in
`arduino_read_tempsensors.ino`. It reads out all of the sensors serially over few
seconds. This data is provided over a serial connection. The program
`read_temperatures.py` listens to this serial data and saves it into
a csv file. `plot_temperatures.py` reads that file and creates a plot
and updates it as a function of time. There is one temperature
sensor for each led and one for the front plate and one for the
base plate.

LabJack
-------
The labjack is connected to the resistors for each led as well as the
pressure gauge. The program `read_analog.py` controls the labjack,
reads out the data and saves it to a csv file. The program
`plot_pressure.py` reads that csv file and creates two plots one of the
pressure and one with all of voltages across the resistors as a
function of time.

LED Brightnesses
----------------
LEDs are measured as a function of time with a camera taking at
regular intervals. The program `snap.py` controls the camera and takes
a set of exposures. These photo set are stored in `snaps/working/`.
The program then waits until taking the next set. The program
`image_check_loop.py` processes each of the images and moves the
images to the appropriate monthly folder. It location of each LED are
stored in the file ``. This program stores the brightnesses in a csv
file. The program `plot_leds.py` reads that file and creates a plot
of the brightness as a function of time. It will average the brightness
for each LED at the difference exposures to display only one point
per snap exposure set.
