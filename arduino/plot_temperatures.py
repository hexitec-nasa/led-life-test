# -*- coding: utf-8 -*-
"""
Plots the temperatures stored in temperatures_csv

Created on Mon Jul 10 15:15:38 2017
@author: schriste
"""
import pandas as pd
import os.path
import time
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import glob
from pandas.io.common import EmptyDataError

from ledlifetest import data_dir, plot_temperature_update_s,\
    number_of_points_to_plot, linedisplay, time_format, setup_logging

# translate from arduino to drawing LED numbers
legend = ['16', '17', '18', '13', '5', 'Front plate', 'Base plate',
          '2', '9', '1', '12', '11', '10', '14', '15', '6', '7', '8', '3', '4']

plot_filename = os.path.join(data_dir, 'temperatures_plot.pdf')

log = setup_logging(os.path.join(data_dir, 'log_plot_temperature.log'))

def plot_temperatures(df):
    """
    Plot LED flux against image creation date for last months worth of data
    """

    plt.figure(figsize=(10, 10))
    times = df.index[-number_of_points_to_plot:]
    for column, label in zip(df, legend):
        y = df[column][-number_of_points_to_plot:]
        plt.plot(times, y, linedisplay[label][0], color=linedisplay[label][1],
                 label=label)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(time_format))
    plt.gcf().autofmt_xdate()
    plt.grid(True)
    plt.ylabel('Temperature [C]')
    plt.ylim(15, 80)
    plt.title('Last Update {0}'.format(times[-1].strftime(time_format)))
    plt.savefig(plot_filename)
    plt.close()


while True:
    # find the latest temperature data
    newest = max(glob.iglob(os.path.join(data_dir,'led_temperatures_*.csv')), key=os.path.getctime)
    # create dataframe of csv data
    try:
        df = pd.read_csv(newest, index_col='time', parse_dates=True)
        log.info("Creating new plot")
        plot_temperatures(df)
    except EmptyDataError:
        pass
    log.info("Sleeping for {0} s.".format(plot_temperature_update_s))
    time.sleep(plot_temperature_update_s)
