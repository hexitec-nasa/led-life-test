import os.path
import logging
import sys
from datetime import datetime
import glob
import numpy as np

# working directory path
here = os.path.abspath(os.path.dirname(__file__))
# data_dir = os.path.join(os.path.dirname(here), 'ledlifetest/camera')
data_dir = '/home/x-ray-lab/Data/ledlifetest/working/'
snaps_data_dir = os.path.join(data_dir, 'snaps/')
# snaps are created here by snap.py and then moved by image_check_loop
working_snaps_data_dir = os.path.join(snaps_data_dir, 'working/')

number_of_points_to_plot = 1000

linedisplay = {'1': ['--', 'b'], '2': ['--', 'g'], '3': ['--', 'r'], # group A
               '4': ['--', 'c'], '5': ['--', 'm'], '6': ['--', 'y'], # group A
               '7': ['-', 'b'], '8': ['-', 'g'], '9': ['-', 'r'], # group B
               '10': ['-', 'c'], '11': ['-', 'm'], '12': ['-', 'y'], # group B
               '13': ['-.', 'b'], '14': ['-.', 'g'], '15': ['-.', 'r'], # group C
               '16': ['-.', 'c'], '17': ['-.', 'm'], '18': ['-.', 'y'], # group C
               'Front plate': ['-', 'k'], 'Base plate': ['--', 'k'], # for heaters
               '+5V Supply': ['-', 'k'], 'pressure': ['-', 'k']} # for analog

# set of exposure times where each LED has at least one exposure time where
# its brightness is around 200
# the 10000 exposure is used to find the LED regions
#snap_exposures = np.arange(40, 1000, 40)
#snap_exposures = np.append(snap_exposures, np.arange(1000, 2000, 200))
#snap_exposures = np.append(snap_exposures, [7000, 10000])
snap_exposures = [140, 420, 620, 1220, 2100, 3180, 6500, 10000]

# file where LED bounding boxes are stored
snap_led_boxes_file = os.path.join(data_dir, 'LEDboxes.txt')
# file where the LED brightnesses are saved
snap_led_csv_file = os.path.join(data_dir, 'snap_check_loop.csv')

# define the update times for each program
snap_update_s = 1 * 60 * 60 # one hour
image_check_loop_update_s = snap_update_s
plot_led_update_s = snap_update_s / 2.
plot_temperature_update_s = snap_update_s
find_regions_update_s = 24 * 60 * 60 # one day
analog_update_s = 60

# define the arduino update rate, hard coded in arduino_read_tempsensors.ino
arduino_update_s = 30

# test set
snap_update_s = 60 * 60
image_check_loop_update_s = 60 * 10
plot_led_update_s = 60 * 15
plot_temperature_update_s = 60 * 10
find_regions_update_s = 60
analog_update_s = 60 * 60

num_leds = 18
num_snaps = len(snap_exposures)

time_format = "%Y-%m-%d %H:%M:%S"

def setup_logging(filepath):
    """
    Setup up logging to a file as well as to the screen
    """
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    formatter = logging.Formatter('[%(asctime)s - %(filename)s:%(lineno)s]:%(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = logging.FileHandler(filepath)
    fh.setFormatter(formatter)
    log.addHandler(fh)

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    log.addHandler(ch)

    return log


def load_box_data(filename):
    """
    Load bounding boxes from LEDboxes.txt for use in LedImage class
    """
    boxes = np.loadtxt(snap_led_boxes_file, delimiter=',')
    # safeguard to turn 1d array into 2d array if LEDboxes.txt just contains
    # one row (happens whens are off)
    if len(boxes.shape) == 1:
        boxes = np.reshape(boxes, (1, 4))
    return boxes


# def find_latest_snaps(time_diff_allowed_s):
#     """
#     Find the last batch of images taken by the camera. Latest snaps
#     """
#     folder_name = find_this_months_folder()
#     files = sorted(
#         glob.iglob(os.path.join(folder_name, '*.fits')),
#         key=os.path.getctime)
#     latest_files = files[-num_snaps:]
#     time_diff = np.array([(current_time - datetime.fromtimestamp(os.path.getctime(f))).total_seconds() for f in latest_files])
#     if np.all(time_diff > time_diff_allowed_s):  # how fresh files must be compared to current time to be considered a new set of snaps
#         return latest_files
#     else:
#         return None

def search_snaps(exposure):
    """Given an exposure time, return the latest image with that exposure time.
    """
    folder_name = find_this_months_folder()
    files = sorted(
        glob.iglob(os.path.join(folder_name, '*-{0}usec.fits'.format(exposure))),
        key=os.path.getctime)
    return files[-1]


def find_this_months_folder():
    """
    Return this month's snap directory
    """
    current_time = datetime.now()
    folder_name = os.path.join(snaps_data_dir, '{0}/'.format(current_time.strftime('%Y-%m')))
    if not os.path.isdir(folder_name):
        log.info('{0} folder not found, creating it.'.format(folder_name))
        os.mkdir(folder_name)
    return folder_name


def create_time_string(this_time):
    return str(this_time).replace('-','').replace(' ','_').replace(':','')[:-7]
