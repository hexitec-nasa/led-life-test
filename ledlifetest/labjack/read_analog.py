from labjack import ljm
import numpy as np
from datetime import datetime
import time
import shutil
import os.path
# Open first found LabJack

from ledlifetest import data_dir, create_time_string, setup_logging,\
    create_time_string, analog_update_s

# Call eReadName to read the serial number from the LabJack.


def connect_to_labjack():
    handle = ljm.openS("ANY", "ANY", "ANY")
    name = "SERIAL_NUMBER"
    result = ljm.eReadName(handle, name)
    handle = ljm.openS("T7", "USB", "ANY")
    print("\neReadName result: ")
    print("    %s = %f" % (name, result))
    return handle

#voltages = np.zeros(80)
start_addr = 48
end_addr = 128
read_count = 0
filename = os.path.join(data_dir, 'resistors_data.csv')
time_string = create_time_string(datetime.now())
backup_filename = os.path.join(data_dir, 'resistors_data_{0}.csv'.format(time_string))

log = setup_logging(os.path.join(data_dir, 'log_read_analog.log'))

log.info("Creating file {0}".format(filename))

addr_list = [62, 64, 65, 66, 67, 68, 69, 70, 71, 86, 88, 89, 90, 91,
             92, 93, 94, 95]
# pressure reading
addr_list.append(0)
# +5V supply
addr_list.append(63)

addr_str_list = ["AIN" + str(addr) for addr in addr_list]


def convert_to_pressure(analog_value):
    """Calculates the pressure in mTorr"""
    return 10.0 ** (analog_value - 5) * 1000

pressure_log = os.path.join(data_dir, 'pressure_data.csv')
pressure_log_backup = os.path.join(data_dir, 'pressure_data_{0}.csv'.format(time_string))

pressure_file = open(pressure_log, 'w')
pressure_file.write('time, pressure[mtorr]\n')

with open(filename, 'w') as csvfile:
    col_names = 'time'
    for addr_str in addr_str_list:
        col_names += ',' + addr_str
    csvfile.write(col_names + '\n')
    while True:
        try:
            handle = connect_to_labjack()
            current_time_string = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            csvfile.write(current_time_string)
            for addr_str in addr_str_list:
                voltage = ljm.eReadName(handle, addr_str)
                #voltages[i-start_addr] = voltage
                log.info("{0}: {1}".format(addr_str, voltage))
                csvfile.write(',' + str(voltage))
                if addr_str == 'AIN0':
                    current_pressure = convert_to_pressure(voltage)
                    log.info("Current pressure: {0} mTorr, {1} Torr.".format(current_pressure, current_pressure / 1e-3))
                    pressure_file.write('{0}, {1}\n'.format(current_time_string, current_pressure))
                    pressure_file.flush()
            csvfile.write('\n')
            csvfile.flush()
            log.info('Backup file')
            shutil.copy(filename, backup_filename)
        except ljm.LJMError:
            log.info("Could not connect to labjack.")
            pass
        time.sleep(analog_update_s)

