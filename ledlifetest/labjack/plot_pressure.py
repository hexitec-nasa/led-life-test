# -*- coding: utf-8 -*-
"""
Plots the temperatures stored in temperatures_csv

Created on Mon Jul 10 15:15:38 2017
@author: schriste
"""
import pandas as pd
import os.path
import time
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import glob
from pandas.io.common import EmptyDataError

from ledlifetest import data_dir, number_of_points_to_plot, setup_logging,\
    plot_temperature_update_s, time_format, linedisplay

plot_filename = os.path.join(data_dir, 'pressure_plot.pdf')

log = setup_logging(os.path.join(data_dir, 'log_plot_analog_pressure.log'))

# translate from labjack values to drawing LED numbers
legend = {'AIN62':'9', 'AIN63': '+5V Supply', 'AIN64': '7', 'AIN65': '8', 'AIN66': '5',
          'AIN67': '6', 'AIN68': '3', 'AIN69': '4', 'AIN70': '1', 'AIN71': '2',
          'AIN94': '10', 'AIN92': '12', 'AIN90': '14', 'AIN88': '16', 'AIN86':'18',
          'AIN95': '11', 'AIN93': '13', 'AIN91': '15', 'AIN89': '17',
          'AIN0': 'pressure'}


def plot_pressure(df):
    """
    Plot pressure as a function of time
    """
    plot_filename = os.path.join(data_dir, 'pressure_plot.pdf')

    plt.figure(figsize=(10, 10))
    times = df.index[-number_of_points_to_plot:]
    y = df[df.columns[0]][-number_of_points_to_plot:]
    plt.plot(times, y)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(time_format))
    plt.gcf().autofmt_xdate()
    plt.grid(True)
    plt.ylabel('Pressure [mTorr]')
    plt.yscale('log')
    plt.ylim(1, 5e4)
    plt.title('Last Update {0}'.format(times[-1].strftime(time_format)))
    plt.savefig(plot_filename)
    plt.close()


def plot_analog(df):
    """
    Make the analog plot.

    :param df:
    :return:
    """
    analog_plot_filename = os.path.join(data_dir, 'analog_plot.pdf')

    plt.figure(figsize=(10, 10))
    times = df.index[-number_of_points_to_plot:]
    for column in df:
        label = legend.get(column, '')
        y = df[column][-number_of_points_to_plot:]
        plt.plot(times, y, linedisplay[label][0], color=linedisplay[label][1],
                 label=label)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(time_format))
    plt.gcf().autofmt_xdate()
    plt.grid(True)
    plt.ylabel('Voltage [V]')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.title('Last Update {0}'.format(times[-1].strftime(time_format)))
    plt.savefig(analog_plot_filename)
    plt.close()

while True:
    # find the latest temperature data
    newest = max(glob.iglob(os.path.join(data_dir,'pressure*.csv')), key=os.path.getctime)
    # create dataframe of csv data
    try:
        df = pd.read_csv(newest, index_col='time', parse_dates=True)
        log.info("Creating new pressure plot")
        plot_pressure(df)
    except EmptyDataError:
        pass

    newest = max(glob.iglob(os.path.join(data_dir,'resistors_data_*.csv')), key=os.path.getctime)
    try:
        df = pd.read_csv(newest, index_col='time', parse_dates=True)
        log.info("Creating new resistor plot")
        plot_analog(df)
    except EmptyDataError:
        pass

    log.info("Sleeping for {0} s".format(plot_temperature_update_s))
    time.sleep(plot_temperature_update_s)
