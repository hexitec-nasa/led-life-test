# -*- coding: utf-8 -*-
"""
This program loads the latest images created by snap.py, loads each of them
and adds their parameters to a csv file (snap_check_loop.csv). These
parameters include the time of each file, the filename and the brightness
of each LED. This looks in the snaps working directory adds the files to the
csv and then moves them to their correct location.

Created on Thu Jun 08 10:10:49 2017
@author: Ian Harper
"""
import os
from datetime import datetime
import time
import numpy as np
import glob
import shutil

from ledlifetest import num_snaps, load_box_data, find_this_months_folder, \
    snap_led_boxes_file, num_leds, working_snaps_data_dir, snap_led_csv_file,\
    image_check_loop_update_s, setup_logging, data_dir, create_time_string,\
    time_format

from ledlifetest.camera.led import LedImage

time_string = create_time_string(datetime.now())
backup_filename = os.path.join(data_dir, '{0}_{1}.csv'.format(snap_led_csv_file, time_string))

log = setup_logging(os.path.join(data_dir, 'log_image_check_loop.log'))

# write headers of csv
with open(snap_led_csv_file, 'w+') as g:
    g.write('time,filename,exposuretime(usec),background')
    for i in range(num_leds):
        g.write(',region{0}'.format(i+1))
    g.write('\n')

    # initialise array to store previous led brightnesses
    led_snap_brightness = np.zeros((num_snaps, num_leds))

    log.info("loop started")

    while True:
        # assign bounding boxes to be used in LedImage()
        boxes = load_box_data(snap_led_boxes_file)
        # look for new images stored in working directory
        files = sorted(glob.iglob(os.path.join(working_snaps_data_dir, '*usec.fits')), key=os.path.getctime)

        save_folder_name = find_this_months_folder()
        if not os.path.isdir(save_folder_name):
            log.info('Creating folder {0}'.format(save_folder_name))
            os.mkdir(save_folder_name)

        if (files is None) or (len(files) < num_snaps):
            log.info('waiting for snap.py or still running')
        else:
            log.info('Found new files {0}\n'.format(list(os.path.basename(f) for f in files)))    # make more readable
            # go through each file
            for i, f in enumerate(files):
                image = LedImage(f, box=boxes)
                maxb = image.get_led_max()
                t = datetime.fromtimestamp(os.path.getmtime(image.filename)).strftime(time_format)
                log.info('Processing file created at {0}'.format(t))
                g.write('{0}, {1}, {2}, {3}'.format(t,
                                                    os.path.basename(image.filename),
                                                    image.exposure_time,
                                                    image.get_BKG()))
                # go through each LED in this file

                # make a plot of each image
                image.plot_image(filename=os.path.join(data_dir, 'latest_{0}.png'.format(os.path.basename(image.filename).split('-')[1].split('.')[0])))

                for j, led in enumerate(maxb):
                    flux = led / image.exposure_time
                    g.write(', {0}'.format(led))
                    # check if any LEDs have changed brightness
                    #try:
                    #    np.testing.assert_allclose(led, led_snap_brightness[i][j], rtol=0.05, atol=10)
                    #except AssertionError:
                    #    log.info("LED {0} has changed brightness from {1} to {2} in file {3}".format(j+1, led_snap_brightness[i][j], led, os.path.basename(image.filename)))
                        # store new brightnesses to check for next time
                    #    led_snap_brightness[i][j] = led
                g.write('\n')
                g.flush()

                # now move file into its permanent location
                shutil.move(f, save_folder_name)

                # make a copy of the csv for record
                # copyfile("snap_check_loop.csv", "snap_check_loop_from_{0}.csv".format(datetime.now().strftime("%Y-%m")))
        # backup the file
        log.info('backing up file')
        shutil.copy(snap_led_csv_file, backup_filename)
        log.info("end of loop, sleeping for {0} seconds".format(image_check_loop_update_s))
        time.sleep(image_check_loop_update_s)
