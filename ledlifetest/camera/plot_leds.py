# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 15:15:38 2017

@author: Ian
"""
import pandas as pd
import os.path
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.io.common import EmptyDataError

from ledlifetest import data_dir, snap_led_csv_file,\
    plot_led_update_s, number_of_points_to_plot, linedisplay, setup_logging,\
    time_format

# translate from region labels to drawing LED numbers
legend = ['13', '12', '15', '10', '14', '16', '11', '9', '17', '8', '18', '2',
          '5', '7', '1', '6', '3', '4']

log = setup_logging(os.path.join(data_dir, 'log_plot_leds.log'))


def plot_leds_vs_date(df, num_points=1000, filename='led_plot.pdf'):
    """
    Plot LED flux against image creation date for last months worth of data
    """
    plt.figure(figsize=(10, 10))

    # Iterate through just the region columns
    for led_num, region_str in zip(legend, df.iloc[:, 3:]):
        # remove all overexposed values
        this_series = df[region_str].copy()
        these_exposures_durations = df['exposuretime(usec)']
        # remove the overexposed values
        this_series[this_series >= 255] = np.nan
        # remove the underexposed values
        this_series[this_series <= 50] = np.nan
        this_flux = this_series / these_exposures_durations
        # average all of the different exposure times into one flux val
        this_flux_mean = this_flux.resample('10T').mean()
        this_flux_mean = this_flux_mean.dropna()
        if len(this_flux_mean) > num_points:
            y = this_flux_mean[-num_points:].values
            t = this_flux_mean[-num_points:].index
        else:
            y = this_flux_mean.values
            t = this_flux_mean.index
        plt.plot(t, y, linedisplay[led_num][0], color=linedisplay[led_num][1],
                 label="LED {0}".format(led_num))

    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(time_format))
    plt.gcf().autofmt_xdate()
    plt.ylabel('LED flux [dn/usec]')
    plt.title('Last Update {0}'.format(t[-1].strftime(time_format)))
    plt.grid(True)
    plt.savefig(os.path.join(data_dir, filename))
    plt.close()


def plot_leds_vs_exptime(df):
    """
    Plot LED brightness against exposure time
    """
    # Iterate through just the region columns
    for led_num, region_str in zip(legend, df.iloc[:, 3:]):
        fig = plt.figure(figsize=(10, 10))

        # remove all overexposed values
        this_series = df[region_str].copy()
        these_exposures_durations = df['exposuretime(usec)']
        # remove the overexposed values
        this_series[this_series >= 255] = np.nan
        # remove the underexposed values
        this_series[this_series <= 5] = np.nan

        x = these_exposures_durations.values
        y = this_series.values
        plt.plot(x, y, linedisplay[led_num][0] + 'o', color=linedisplay[led_num][1],
                 label="LED {0}".format(led_num))
        plt.title("{0}, LED {1}".format(region_str, led_num))
        plt.ylabel('DN')
        plt.grid(True)
        plt.xlabel('Exposure time')

        plt.savefig(os.path.join(data_dir, '{0}_exp_vs_dn.pdf'.format(region_str)))
        plt.close()

while True:
    # find the latest led data
    #newest = max(glob.iglob(os.path.join(data_dir, 'snap_check_loop.csv')),
    #             key=os.path.getctime)
    # create dataframe of csv data
    i = 0
    try:
        df = pd.read_csv(snap_led_csv_file, index_col='time', parse_dates=True)
        log.info("Creating new plot")
        plot_leds_vs_date(df, num_points=number_of_points_to_plot)
        plot_leds_vs_date(df, num_points=0, filename='led_plot_all.pdf')
        # every once in a while make the exposure time plot
        if (i % 10) == 0:
            plot_leds_vs_exptime(df)
        i += 1
    except EmptyDataError:
        pass
    log.info("Sleeping for {0} s.".format(plot_led_update_s))
    time.sleep(plot_led_update_s)
