"""
Continously takes images and prints out the brightness values
DO NOT run this while snap.py or image_check_loop.py is running!
"""

import os
import time
import glob

from ledlifetest.camera.led import LedImage
from ledlifetest import working_snaps_data_dir

CAMERA_SNAP_DELAY_S = 1   # camera delay between snapshots

# pathname of C++ program that takes photos
SNAP_EXE = '/home/x-ray-lab/Desktop/SAAS-master/snap2'
SNAP_IP = '192.168.4.4'    # IP address of camera
EXPOSURE = 10000
ANALOG_GAIN = 200     # gain setting on camera
PREAMP_GAIN = 0   # needs to be -3, 0, 3, or 6

while True:
    
    # make temporary folder for snaps and change into it
    if not os.path.isdir(working_snaps_data_dir):
        log.info('Creating folder {0}'.format(working_snaps_data_dir))
        os.mkdir(working_snaps_data_dir)

    os.chdir(working_snaps_data_dir)

    # check how many files exist now
    number_of_files = len(glob.glob(os.path.join(working_snaps_data_dir, '*.fits')))

    snap_command_string = "{0} {1} {2} {3} {4}".format(SNAP_EXE, SNAP_IP, EXPOSURE, ANALOG_GAIN, PREAMP_GAIN)
    os.system(snap_command_string)

    new_number_of_files = len(glob.glob(os.path.join(working_snaps_data_dir, '*.fits')))

    # if a new file was created then we are good
    if (new_number_of_files - number_of_files) == 1:
        latest_file = max(
            glob.iglob(os.path.join(working_snaps_data_dir, '*.fits')),
            key=os.path.getctime)

        image = LedImage(latest_file)
        image.find_LEDs()
        maxb = image.get_led_max()
        print('**********')
        print(', '.join('{:3d}'.format(int(b)) for b in maxb))
        print('**********')
        del(image)

        os.remove(latest_file)

    time.sleep(CAMERA_SNAP_DELAY_S)
