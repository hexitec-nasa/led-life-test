# -*- coding: utf-8 -*-
"""
This program reads images generates by snap.py and automatically finds the
LED locations. Locations are defined as the four corners of a rectangle
containing the LED. They are stored in a text file (LEDboxes.txt) which
is used by image_check_loop. If the LEDs have moved more than
LED_MOVE_TOLERANCE, a warning is issued to the log and the new locations
are stored in LEDboxes.txt.

@author: Ian
"""
import os
import numpy as np
import time
from datetime import datetime

from ledlifetest.camera.led import LedImage
from ledlifetest import snap_exposures, search_snaps, data_dir, \
    load_box_data, snap_led_boxes_file, find_regions_update_s

# centers of bounding boxes are allowed to move within this number of pixels
#  before issuing a warning and storing the new locations.
LED_MOVE_TOLERANCE = 10

# preview image is stored here
image_filename = 'LEDboxes_image.png'
image_file = os.path.join(data_dir, image_filename)

while True:
    f = search_snaps(snap_exposures.max())   # get the last most exposed photo
    if f is not None:
        image = LedImage(f)
        image.find_LEDs()
        LEDregions = image.label_image
        these_LEDboxes = image.bounding_box
        maxb = image.get_led_max()
        image.plot_image(filename=os.path.join(data_dir,
                                               'latest_{0}.png'.format(
                                                   os.path.basename(
                                                       image.filename).split(
                                                       '-')[1].split('.')[0])))

        # check if regions have changed
        region_moved = False
        # load previous box regions
        boxes = load_box_data(snap_led_boxes_file)
        # calculate their centers
        x_centers = np.average([boxes[:, 0], boxes[:, 2]], axis=0)
        y_centers = np.average([boxes[:, 1], boxes[:, 3]], axis=0)
        box_centers = np.array([x_centers, y_centers]).transpose().shape

        for j in range(image.num_leds_found):
            try:
                np.testing.assert_allclose(image.box_centers[j],
                                           box_centers[j],
                                           atol=LED_MOVE_TOLERANCE)
            except AssertionError:
                region_moved = True
                log.info("LED {0} has moved from {1} to {2} in file {3}".format(j+1, box_centers[j], image.box_centers[j], os.path.basename(image.filename)))
                box_centers[j] = image.box_centers[j]  # update location of the bounding box centers

        # store regions in txt file
        if region_moved:
            time_string = datetime.now().isoformat().replace('-', '')
            this_snap_led_boxes_file = os.path.join(data_dir, 'LEDboxes_{0}'.format(time_string))
            # save a time tagged file
            np.savetxt(this_snap_led_boxes_file, np.array(these_LEDboxes), fmt='%i', delimiter=',')
            # overwrite existing file
            np.savetxt(snap_led_boxes_file, np.array(these_LEDboxes), fmt='%i', delimiter=',')

        log.info("regions found, checking again in {0} seconds".format(UPDATE_TIME_S))
        log.info("sleeping...")
        time.sleep(find_regions_update_s)
    else:
        log.info('No snaps found, checking again in 30 seconds')
        time.sleep(30)
