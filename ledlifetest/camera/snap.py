""" takes a batch of images with different exposure times and puts images in monthly folder"""

import os
import shutil
import time
import logging
import sys
import glob
from datetime import datetime

from ledlifetest import snap_exposures, data_dir, working_snaps_data_dir,\
    snap_update_s, setup_logging

log = setup_logging(os.path.join(data_dir, 'log_snap.log'))

CAMERA_SNAP_DELAY_S = 5   # camera delay between snapshots

# pathname of C++ program that takes photos
SNAP_EXE = '/home/x-ray-lab/Desktop/SAAS-master/snap2'
SNAP_IP = '192.168.4.4'    # IP address of camera
ANALOG_GAIN = 200     # gain setting on camera
PREAMP_GAIN = 0   # needs to be -3, 0, 3, or 6

snap_count = 0

while True:
    current_time = datetime.now()
    log.info('Saving inside {0} folder.'.format(working_snaps_data_dir))

    # make temporary folder for snaps and change into it
    if not os.path.isdir(working_snaps_data_dir):
        log.info('Creating folder {0}'.format(working_snaps_data_dir))
        os.mkdir(working_snaps_data_dir)

    os.chdir(working_snaps_data_dir)

    # take photos
    for exp in snap_exposures:
        list_of_files = []
        snap_created = False
        while not snap_created:
            """
            if for some reason a photo is not taken, this loop will 
            attempt to take the photo again and again until it is taken
            """
            # check how many files exist now
            number_of_files = len(glob.glob(
                os.path.join(working_snaps_data_dir, '*[0-9].fits')))
            snap_command_string = "{0} {1} {2} {3} {4}".format(SNAP_EXE, SNAP_IP, exp, ANALOG_GAIN, PREAMP_GAIN)
            os.system(snap_command_string)

            # check how many files exist now
            new_number_of_files = len(glob.glob(
                os.path.join(working_snaps_data_dir, '*[0-9].fits')))
            if (new_number_of_files - number_of_files) == 1:
                # if a new file was created then we are good
                snap_created = True
            else:
                log.info("Snap failed. Trying again.")

            log.info("Sleeping before next snap...")
            # wait for the camera to be ready again
            time.sleep(CAMERA_SNAP_DELAY_S)

        log.info(snap_command_string)
        log.info('Snap {0} exposure={1} usec'.format(snap_count, str(exp)))
        snap_count += 1

        # rename file to include exposure time
        latest_file = max(
            glob.iglob(os.path.join(working_snaps_data_dir, '*[0-9].fits')),
            key=os.path.getctime)
        renamed_file = "{0}-{1}usec.fits".format(latest_file.split('.')[0], str(exp))
        log.info("Renaming file {0} to {1}".format(latest_file, renamed_file))
        os.rename(latest_file, renamed_file)
        # move image to monthly folder
        # shutil.move(renamed_file, save_folder_name)

    # print how long taking the photos took
    # (use this to set appropriate sleep times for
    # find_regions.py and image_check_loop.py)
    log.info('Taking all snaps took {0} seconds.'.format(datetime.now() - current_time))
    log.info("Finished set. Sleeping for {0} seconds".format(snap_update_s))
    log.info("Snap count is {0}".format(snap_count))
    time.sleep(snap_update_s)
