# -*- coding: utf-8 -*-
"""
This program reads images generates by snap.py and automatically finds the
LED locations. Locations are defined as the four corners of a rectangle
containing the LED. They are stored in a text file (LEDboxes.txt) which
is used by image_check_loop. If the LEDs have moved more than
LED_MOVE_TOLERANCE, a warning is issued to the log and the new locations
are stored in LEDboxes.txt.

@author: Ian
"""
import os
import numpy as np
import shutil
from datetime import datetime

from ledlifetest.camera.led import LedImage
from ledlifetest import snap_exposures, search_snaps, data_dir, \
    load_box_data, snap_led_boxes_file, create_time_string

# centers of bounding boxes are allowed to move within this number of pixels
#  before issuing a warning and storing the new locations.
LED_MOVE_TOLERANCE = 10

# preview image is stored here
image_basename = 'find_regions_image.png'
image_file = os.path.join(data_dir, image_basename)

f = search_snaps(max(snap_exposures))   # get the last most exposed photo
if f is not None:
    print("Using file {0}".format(os.path.basename(f)))
    image = LedImage(f)
    print("Exposure time is {0} us".format(image.exposure_time))
    image.find_LEDs()
    LEDregions = image.label_image
    these_LEDboxes = image.bounding_box
    maxb = image.get_led_max()
    image.plot_image(filename=image_file)

    time_string = create_time_string(datetime.now())
    backup_snap_led_boxes_file = os.path.join(data_dir, 'LEDboxes_{0}.txt'.format(time_string))
    np.savetxt(snap_led_boxes_file, np.array(these_LEDboxes), fmt='%i', delimiter=',')
    shutil.copy(snap_led_boxes_file, backup_snap_led_boxes_file)

